﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Library_Management_System
{
    public partial class frmBook : Form
    {
        bool b; string status; 
        public frmBook()
        {
            InitializeComponent();
        }
        void columnheader() {
            listView1.Columns.Add("BookID", listView1.Width / 6);
            listView1.Columns.Add("Book Title", listView1.Width / 6);
            listView1.Columns.Add("Category", listView1.Width / 6);
            listView1.Columns.Add("", 0);
            listView1.Columns.Add("", 0);
            listView1.Columns.Add("Athor", listView1.Width / 6);
            listView1.Columns.Add("Publisher", listView1.Width / 6);
            listView1.Columns.Add("", 0);
            listView1.Columns.Add("Qaulity", listView1.Width / 6);
            listView1.Columns.Add("", 0);
            listView1.Columns.Add("", 0);
            listView1.Columns.Add("", 0);
            listView1.View = View.Details;
            listView1.FullRowSelect = true;
        }
        void openfile() {
            FileStream fs = new FileStream("Bookinfo.txt", FileMode.Open);
            BinaryFormatter Bf = new BinaryFormatter();
            listView1.Items.Clear();
            ListViewItem T;
            while (fs.Position != fs.Length)
            {
                T = (ListViewItem)Bf.Deserialize(fs);
                listView1.Items.Add(T);
            }
            fs.Close();
        }
        private void frmBook_Load(object sender, EventArgs e)
        {
            columnheader();
            openfile();
            enableText(false);
            btnsaveeditdelete(false);
        }

        private void listView1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            status = "new";
            enableText(true);
            clearText();
            txtBookTitle.Focus();
            btnsaveeditdelete(false);
            btnAdd.Enabled = true;
            btnNew.Enabled = false;            
        }
        void clearText() {
            txtBookID.Clear();
            txtBookTitle.Clear();
            cboCategory.Text = "";
            cboLang.Text = "";
            txtISBN.Clear();
            cboAthorName.Text = "";
            cboPubName.Text = "";
            cboSupName.Text = "";
            txtQty.Clear();
            txtPrice.Clear();
            txtStore.Clear();
        }        
        void enableText(bool b)
        {
            txtBookID.Enabled = false;
            txtBookTitle.Enabled = b;
            cboCategory.Enabled = b;
            cboLang.Enabled = b;
            txtISBN.Enabled = b;
            cboAthorName.Enabled = b;
            cboPubName.Enabled = b;
            cboSupName.Enabled = b;
            txtQty.Enabled = b;
            txtPrice.Enabled = b;
            txtStore.Enabled = b;
        }
        void checkNull()
        {
            if (txtBookID.Text == "")
            {
                MessageBox.Show("Plese Fill Book ID");
                b = false;
                txtBookID.Focus();
            }
            else if (txtBookTitle.Text == "") 
            {
                MessageBox.Show("Please Fill Book Title");
                b = false;
                txtBookTitle.Focus();
            }
            else if (cboCategory.Text == "")
            {
                MessageBox.Show("Please Selete Category");
                b = false;
                cboCategory.Focus();
            }
            else if (cboLang.Text == "")
            {
                MessageBox.Show("Please Selete Language");
                b = false;
                cboLang.Focus();
            }
            else if (txtISBN.Text == "")
            {
                MessageBox.Show("Please Fill ISBN");
                b = false;
                txtISBN.Focus();
            }
            else if (cboAthorName.Text == "")
            {
                MessageBox.Show("Please Selete Athor Name");
                b = false;
                cboAthorName.Focus();
            }
            else if (cboPubName.Text == "")
            {
                MessageBox.Show("Please Selete Publisher Name");
                b = false;
                cboPubName.Focus();
            }
            else if (cboSupName.Text == "")
            {
                MessageBox.Show("Please Selete Supplier Name");
                b = false;
                cboSupName.Focus();
            }
            else if (txtQty.Text == "")
            {
                MessageBox.Show("Please Fill Quality");
                b = false;
                txtQty.Focus();
            }
            else if (txtPrice.Text == "")
            {
                MessageBox.Show("Please Fill Price");
                b = false;
                txtPrice.Focus();
            }
           // else if (picBook.Image == null)
           // {
           //     MessageBox.Show("Please select Picture of Book");
           //     b = false;
           //     btnBrowse.Focus();
           // }
            else
                b = true;

        }
        ListViewItem addBook() {
            ListViewItem T;
            T = listView1.Items.Add(txtBookID.Text);
            T.SubItems.Add(txtBookTitle.Text);
            T.SubItems.Add(cboCategory.Text);
            T.SubItems.Add(cboLang.Text);
            T.SubItems.Add(txtISBN.Text);
            T.SubItems.Add(cboAthorName.Text);
            T.SubItems.Add(cboPubName.Text);
            T.SubItems.Add(cboSupName.Text);
            T.SubItems.Add(txtQty.Text);
            T.SubItems.Add(txtPrice.Text);
            T.SubItems.Add(txtStore.Text);
            return T;
        }
        void saveBook() {
            ListViewItem T;
            FileStream fs = new FileStream("Bookinfo.txt", FileMode.Create);
            BinaryFormatter Bf = new BinaryFormatter();
            for (int i = 0; i < listView1.Items.Count; i++)
            {
                T = listView1.Items[i];
                Bf.Serialize(fs, T);
            }
            fs.Close();
            btnAdd.Enabled = false;
            btnNew.Enabled = true;
        }            
        private void btnAdd_Click(object sender, EventArgs e)
        {            
            checkNull();
            ListViewItem T;
            if (b == true)
            {
                if (status == "new")
                {
                    T = addBook();                    
                }
                else
                {
                    
                    //FileStream fs = new FileStream("Bookinfo.txt", FileMode.Create);
                    //BinaryFormatter Bf = new BinaryFormatter();                    
                    for (int i = 0; i < listView1.Items.Count; i++)
                    {
                        if (listView1.Items[i].Text == txtBookID.Text)
                        {
                            //listView1.Items.RemoveAt(i);
                            //ListViewItem T1 = addBook();                            
                            T = listView1.Items[i];
                            T.Text = txtBookID.Text;
                            T.SubItems[1].Text = txtBookTitle.Text;
                            T.SubItems[2].Text = cboCategory.Text;
                            T.SubItems[3].Text = cboLang.Text;
                            T.SubItems[4].Text = txtISBN.Text;
                            T.SubItems[5].Text = cboAthorName.Text;
                            T.SubItems[6].Text = cboPubName.Text;
                            T.SubItems[7].Text = cboSupName.Text;
                            T.SubItems[8].Text = txtQty.Text;
                            T.SubItems[9].Text = txtPrice.Text;
                            T.SubItems[10].Text = txtStore.Text;
                            //T.SubItems[11].Text = txtBookTitle.Text;

                        }                        
                    }
                }
                saveBook();
                btnNew.Enabled = true;
                clearText();
                enableText(false);
            }            
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
           
        }

        private void listclick(object sender, EventArgs e)
        {
            ListViewItem T;
            int i = listView1.SelectedIndices[0];
            T=listView1.Items[i];
            txtBookID.Text = T.Text;
            txtBookTitle.Text = T.SubItems[1].Text;
            cboCategory.Text = T.SubItems[2].Text;
            cboLang.Text = T.SubItems[3].Text;
            txtISBN.Text = T.SubItems[4].Text;
            cboAthorName.Text = T.SubItems[5].Text;
            cboPubName.Text = T.SubItems[6].Text;
            cboSupName.Text = T.SubItems[7].Text;
            txtQty.Text = T.SubItems[8].Text;
            txtPrice.Text = T.SubItems[9].Text;
            txtStore.Text = T.SubItems[10].Text;                        
            btnEdit.Enabled = true;
            btnAdd.Enabled = false;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            status = "old";
            btnsaveeditdelete(false);
            btnAdd.Enabled = true;
            enableText(true);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int n = listView1.SelectedIndices.Count;
            for (int i = 0; i < n; i++) {
                listView1.Items.RemoveAt(listView1.SelectedIndices[0]);
            }
            saveBook();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            clearText();
            enableText(false);
            btnNew.Enabled = true;
            btnsaveeditdelete(false);
        }
        void btnsaveeditdelete(bool a) {
            btnAdd.Enabled = a;
            btnDelete.Enabled = a;
            btnEdit.Enabled = a;
        }
     
       
    }
}
